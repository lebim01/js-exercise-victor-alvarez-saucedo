/**
 * i don't use typescript because i want to test the animal kind validation
 */
import { Cat } from './index'

/**
 * Disable console.log & console.error
 */
console.log = () => { }
console.error = () => { }

describe('Cat class', () => {


  const cat = new Cat('any cat', 'white')

  test("validate properties", () => {
    expect(typeof cat.hairColor).toBe("string");
  });

  test("methods definition", () => {
    expect(typeof cat.meow).toBe("function");
  });

  test("readonly property", () => {
    try {
      cat.hairColor = "blue"
    } catch (err) {
      expect(typeof err.message).toBe("string")
    }
    expect(cat.hairColor).toBe("white")
  })

  test("meow works", () => {
    expect(cat.meow()).toBe(`${cat.name} meows`)
  })
})
import Animal from './Animal'

export default class Cat extends Animal<Cat> {
  //readonly hairColor: string;

  get hairColor() { return this.__getHairColor__() }
  set hairColor(value) { throw new Error('hairColor property is read only'); }

  constructor(name: string, hairColor: string) {
    super('cat', name)
    this.__getHairColor__ = function () { return hairColor };
    //this.hairColor = hairColor
  }

  private __getHairColor__() {

  }

  meow(): string {
    return this.doSomething('meows')
  }
}
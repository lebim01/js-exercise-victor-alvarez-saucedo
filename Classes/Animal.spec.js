/**
 * i don't use typescript because i want to test the animal kind validation
 */
import Dog from './Dog';
import Cat from './Cat'
import names1 from './dummy/names1'
import names2 from './dummy/names2'

/**
 * Disable console.log & console.error
 */
console.log = () => { }
console.error = () => { }

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function generateName() {
  const name = names1[getRandomInt(0, names1.length + 1)] + ' ' + names2[getRandomInt(0, names2.length + 1)];
  return name;
}

const getParents = () => {
  return [new Dog(generateName(), 'Golden'), new Dog(generateName(), 'Golden')]
}

const generateIterativeDummyDogsPedigree = (animal, levels = 100) => {
  let _animals_add_parents = [animal]
  let i = 0
  do {
    _animals_add_parents = _animals_add_parents.map((animal) => {
      const parents = getParents()
      animal.addParent(parents[0])
      animal.addParent(parents[1])
      return parents
    }).flat()
    i++
  } while (i < levels);
}

const dog = new Dog('My dog', 'Golden')
generateIterativeDummyDogsPedigree(dog, 22)

describe('Animal class', () => {

  const animal = new Cat('any cat', 'white')

  test("validate properties", () => {
    expect(typeof animal.name).toBe("string");
  });

  test("methods definition", () => {
    expect(typeof animal.sleep).toBe("function");
  })

  test("sleep works", () => {
    expect(animal.sleep()).toBe(`${animal.name} sleeps`)
  })

  test("add parent", () => {
    const cat = new Cat('Toby', 'red')
    cat.addParent(animal)
    expect(cat.parents.length).toBe(1)
  })

  test("validation add parent (different kind)", () => {
    const dog = new Dog('Toby', 'Golden')
    try {
      dog.addParent(animal) // this doesn't works
    } catch (err) {
      expect(typeof err.message).toBe("string")
    }
    expect(dog.parents.length).toBe(0)
  })

  test("print pedigreee", () => {
    const cassandra = new Cat('Cassandra', 'white-black')
    const felix = new Cat('Felix', 'white')
    const pepper = new Cat('Pepper', 'black')

    cassandra.addParent(felix)
    cassandra.addParent(pepper)

    const figaro = new Cat('Figaro', 'white-black')
    const morris = new Cat('Morris', 'white')
    const mongo = new Cat('Mongo', 'black')

    figaro.addParent(morris)
    figaro.addParent(mongo)

    const tiger = new Cat('Tiger', 'white-black')
    tiger.addParent(cassandra)
    tiger.addParent(figaro)

    const pedegree = tiger.printPedigree()
    expect(pedegree).toBe("Felix, Pepper, Morris, Mongo, Cassandra, Figaro, Tiger")
  })

  test("print pedigree stress [iterative]", () => {
    dog.printPedigreeIterative()
    expect(true).toBe(true)
  })

  test("print pedigree stress [recursive]", () => {
    dog.printPedigree()
    expect(true).toBe(true)
  })
})
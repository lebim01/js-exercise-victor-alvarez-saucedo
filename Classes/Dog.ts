import Animal from './Animal'

export default class Dog extends Animal<Dog> {
  //readonly breed: string;

  get breed() { return this.__getBreed__() }
  set breed(value) { throw new Error('breed property is read only'); }

  constructor(name: string, breed: string) {
    super('dog', name)
    this.__getBreed__ = function () { return breed };
    //this.breed = breed
  }

  private __getBreed__() {

  }

  bark(): string {
    return this.doSomething('barks')
  }
}
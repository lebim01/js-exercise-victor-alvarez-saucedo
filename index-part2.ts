/**
For example, let's say we have the following family of cats.

Felix and Pepper are the parents of Cassandra
Morris and Mongo are the parents of Figaro
Cassandra and Figaro are the parents of Tiger
For this family, when printPedigree is called on Tiger, something like the following should be printed.

Felix, Pepper, Morris, Mongo, Cassandra, Figaro, Tiger
 */

import { Cat } from './Classes'

const cassandra = new Cat('Cassandra', 'white-black')
const felix = new Cat('Felix', 'white')
const pepper = new Cat('Pepper', 'black')

cassandra.addParent(felix)
cassandra.addParent(pepper)

const figaro = new Cat('Figaro', 'white-black')
const morris = new Cat('Morris', 'white')
const mongo = new Cat('Mongo', 'black')

figaro.addParent(morris)
figaro.addParent(mongo)

const tiger = new Cat('Tiger', 'white-black')
tiger.addParent(cassandra)
tiger.addParent(figaro)

tiger.printPedigree()
import { Dog, Cat } from './Classes'

const fido = new Dog('Fido', 'Golden Retriever');
fido.sleep(); // prints 'Fido sleeps'
fido.bark(); // prints 'Fido barks'
fido.breed; // returns 'Golden Retriever'

const morris = new Cat('Morris', 'orange');
morris.sleep(); // prints 'Morris sleeps'
morris.meow(); // prints 'Morris meows'
morris.hairColor; // returns 'orange'
/**
 * i don't use typescript because i want to test the animal kind validation
 */
import { Dog } from './index'

/**
 * Disable console.log & console.error
 */
console.log = () => { }
console.error = () => { }

describe('Dog class', () => {

  const dog = new Dog('any dog', 'Golden')

  test("validate properties", () => {
    expect(typeof dog.breed).toBe("string");
  });

  test("methods definition", () => {
    expect(typeof dog.bark).toBe("function");
  });

  test("readonly property", () => {
    try {
      dog.breed = "Husky"
    } catch (err) {
      expect(typeof err.message).toBe("string")
    }
    expect(dog.breed).toBe("Golden")
  })

  test("bark works", () => {
    expect(dog.bark()).toBe(`${dog.name} barks`)
  })
})
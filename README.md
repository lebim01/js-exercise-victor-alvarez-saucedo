# JavaScript Exercise

## Getting started

Create your own [fork of this repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html), and commit all of your work there. When you have completed your solution, committed it, and pushed it up to your fork, [create a pull request](https://www.atlassian.com/git/tutorials/making-a-pull-request) to merge your changes back into this repository.

We probably don't need to say this, but your code will give us an idea of the kind of work we can expect from you, so give us your best!

## Exercise details

### Part 1

Create two classes, one named `Dog` and one named `Cat`. The `Dog` constructor should take two arguments, `name` and `breed`. The `Cat` constructor should take two arguments, `name` and `hairColor`.

```javascript
new Dog(name, breed)
new Cat(name, hairColor)
```

`Dog` and `Cat` should both have a `sleep` method that prints "[Insert name here] sleeps" to the console. All Dogs should have a `bark` method and a **read-only** `breed` property. All Cats should have a `meow` method and a **read-only** `hairColor` property.

Your implementations for `Dog` and `Cat` should give the correct results when the following code is executed.

```javascript
const fido = new Dog('Fido', 'Golden Retriever');
fido.sleep(); // prints 'Fido sleeps'
fido.bark(); // prints 'Fido barks'
fido.breed; // returns 'Golden Retriever'

const morris = new Cat('Morris', 'orange');
morris.sleep(); // prints 'Morris sleeps'
morris.meow(); // prints 'Morris meows'
morris.hairColor; // returns 'orange'
```

### Part 2

All animals can have offspring. Add some functionality to your implementation that allows us to specify parent/child relationships between animals.

Of course, dogs can't have cats as offspring and vice versa, so if we attempt to set up an invalid parent/child relationship, then an error should be thrown.

Also, add a method named `printPedigree` that will print the name of each ancestor of a given animal from furthest ancestor to nearest, including the name of the animal itself.

For example, let's say we have the following family of cats.

* Felix and Pepper are the parents of Cassandra
* Morris and Mongo are the parents of Figaro
* Cassandra and Figaro are the parents of Tiger

For this family, when `printPedigree` is called on Tiger, something like the following should be printed.

> Felix, Pepper, Morris, Mongo, Cassandra, Figaro, Tiger

All grandparents are first, then parents, and finally the animal that `printPedigree` was called on.

### Part 3

Write unit tests for the `Dog` and `Cat` classes using your test framework of choice.


const MAX_PARENTS = 2;

export type AnimalType = 'dog' | 'cat';

type PedigreeTree<T> = { [index: string]: Animal<T>[] }

interface Parents<T> {
  animal: Animal<T>;
  parents: Parents<T>;
}

export default class Animal<T> {
  name: string;
  private kind: AnimalType;
  readonly parents: Animal<T>[] = [];

  constructor(kind: AnimalType, name: string) {
    this.kind = kind
    this.name = name
  }

  protected doSomething(action: string): string {
    const text = `${this.name} ${action}`
    console.log(text)
    return text
  }

  sleep(): string {
    return this.doSomething('sleeps')
  }

  addParent(parent: Animal<T>): void | Error {
    try {
      if (this.parents.length >= MAX_PARENTS) {
        throw new Error(`This animal already has ${MAX_PARENTS} parents associated`)
      }

      /**
       * This guard only is for if using .js because typescript should prevent it
       */
      if (parent.kind !== this.kind) {
        throw new Error(`${parent.name} is not the same kind of ${this.name}, please try associate animal kind ${this.kind}`)
      }

      this.parents.push(parent)
    } catch (err: any) {
      console.error(err.toString())
      throw err
    }
  }

  getPedigreeParents(pedigree_parents: PedigreeTree<T>, index: number, parents: Animal<T>[]): PedigreeTree<T> {
    if (parents.length === 0) return pedigree_parents

    if (!pedigree_parents[index]) pedigree_parents[index] = []

    for (const parent of parents) {
      pedigree_parents[index].push(parent)
    }

    return this.getPedigreeParents(
      pedigree_parents,
      index + 1,
      parents.map(animal => animal.parents).flat()
    )
  }

  printPedigree(): string {
    const tree_parents_pedigree: PedigreeTree<T> = this.getPedigreeParents({}, 0, this.parents)
    const parents_pedigree_array: Animal<T>[] =
      Object.keys(tree_parents_pedigree)
        .map(key => Number(key))
        .sort()
        .reverse()
        .map((key) => tree_parents_pedigree[key])
        .flat()
    const text = [...parents_pedigree_array, this].map(animal => animal.name).join(', ')
    console.info('pedigree length', parents_pedigree_array.length)
    console.log(text)
    return text
  }


  printPedigreeIterative(): string {
    const tree_parents_pedigree: PedigreeTree<T> = {}

    let _parents = this.parents
    let index = 0

    while (_parents.length > 0) {
      if (!tree_parents_pedigree[index]) tree_parents_pedigree[index] = []

      for (const parent of _parents) {
        tree_parents_pedigree[index].push(parent)
      }

      _parents = _parents.map(animal => animal.parents).flat()

      index++
    }

    const parents_pedigree_array: Animal<T>[] =
      Object.keys(tree_parents_pedigree)
        .map(key => Number(key))
        .sort()
        .reverse()
        .map((key) => tree_parents_pedigree[key])
        .flat()
    const text = [...parents_pedigree_array, this].map(animal => animal.name).join(', ')
    console.info('pedigree length', parents_pedigree_array.length)
    console.log(text)
    return text
  }
}